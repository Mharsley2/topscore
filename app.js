const http = require('http');
require("body")
const jsonBody = require("body/json");
let scores = [{name: "Edwin", score: 50}, {name: "David", score: 39}];
const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  if (req.method === "GET") {
    if (req.url != "/scores") {
      res.statusCode = 404;
      res.end("ERROR NOT FOUND");
    } else {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/javascript');
      const responseBody = JSON.stringify (scores);
      res.end(responseBody);
    }
  } else if (req.method === "POST") {
    res.statusCode = 201;
    jsonBody(req, res, (err, body) => {
      console.log(body)
      scores.push (body)
      scores.sort(sortBy("score",true))
      if (scores.length > 3) scores.pop();
      res.end(JSON.stringify(body));
    })
  }
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

const sortBy = function(field, reverse, primer){

  let key = primer ?
      function(x) {return primer(x[field])} :
      function(x) {return x[field]};
 
  reverse = !reverse ? 1 : -1;
 
  return function (a, b) {
      return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
    }
 }

 